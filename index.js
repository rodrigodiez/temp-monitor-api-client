var debug = require('debug');
var log = debug('temp-monitor-api-client');
var error = debug('temp-monitor-api-client:error');
var warn = debug('temp-monitor-api-client:warn');

error.log = console.error.bind(console);
warn.log = console.warn.bind(console);

var request = require('request-promise');
var URI = require('URIjs');

var client = function(options) {

	return {
		notify: function(temperature) {

			var endPoint = URI(options.baseUrl).directory('temperatures').toString();
			var formData = { apiKey: options.apiKey, temperature: temperature };
			
			log('notify URL ', endPoint);
			log('notify data', formData);

			return request({
				uri: endPoint.toString(),
				method: 'POST',
				form: formData
			});
		}
	};
}

module.exports = client;